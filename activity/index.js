let trainer = {

    name: "Ash Ketchun",
    age: 10,
    pokemon: ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {  hoenn: ["May","Max"],
                kanto: ["Brock", "Misty"],
         },
    talk: function(){
    console.log("Pickachu! I choose you!!");

 }

}
console.log(trainer);
console.log("Result of Dot Notation:");
console.log(trainer.name);
console.log("Result of Squire Bracket Notation:");
console.log(trainer.pokemon)
console.log("Result of Talk Method:");
trainer.talk(trainer);





function Pokemon (name, level) {

    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = 1.5 * level;

    
    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
        target.health -= this.attack;
        if (target.health <= 0) {
            target.faint();
        }

}
    this.faint = function( ){
        console.log(this.name + " fainted.")
    }

}

let pickachu = new Pokemon ("Pickachu", 12);
let geodude = new Pokemon ("Geodude",8);
let newto = new Pokemon ("Newto",100);


console.log(pickachu);
console.log(geodude);
console.log(newto);


newto.tackle(geodude);
console.log(geodude);
newto.tackle(geodude);

