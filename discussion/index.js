// console.log("Hello Josh");

// JS Objects is a way to define a real world object with its own characteristic.
// It's also a way to organize data with context through the use of key value.


// let dog = ["loyal",4,"tail","Husky"];


let dog = {

	breed : "Husky",
	color : "White",
	legs	: 4, 
	isHuggable: true,
	isLoyal : true
}
console.log(dog);

let movie = {

	title : "The Three Idiots",
	publisher : "Reliance Entertainment",
	year	: 2009, 
	director : "Rajkumar Hirani",
	isAvailable : true
};
console.log(movie);

// Accessing array items = arrName[index]
// Accessing object properties = objectName.propertyName

console.log(movie.Title)
console.log(movie.Publisher)

movie.year = 2019;
console.log(movie);

movie.title = "Ang probinsyano";
movie.publisher = "FPJ Production";
movie.year = 1972;
console.log(movie);


let course = {

	title : "Philosophy 101",
	description : "Learn the value of life",
	price : 5000,
	isActive : true,
	instructors : ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]

};
console.log(course);

course.instructors.pop();
console.log(course.instructors);

course.instructors.push('Mr. McGee');
console.log(course.instructors);

let isAnInstructor = course.instructors.includes("Mr. Johnson");
console.log(isAnInstructor);

function addNewInstructor (instructor){
	  if (course.instructors.includes(instructor)) {
	  	 console.log("Instructor Already added!");
	  }else{
		course.instructors.push(instructor);
		 console.log("Thank you. Instructor Added!");
}
}
addNewInstructor("Mr. Josue");
addNewInstructor("Mr. Josue");


console.log(course);

// 
let instructor = {};

instructor.name = "James Johnson";
instructor.age = 56,
instructor.gender = "male",
instructor.salary = "50,000",
instructor.department = "Humanities",
instructor.subjects = ["Philosophy",'Humanities', "Logic"]


console.log(instructor);


instructor.address = {
	street:"#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}	

console.log(instructor);
console.log(instructor.address.street);


function Superhero(name,superpower,powerLevel){

	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;


}


let superhero1 = new Superhero ("Saitama", "One Punch", 30000);
console.log(superhero1);



function Laptop (name, brand,price){

	this.name = name;
	this.brand = brand;
	this.price = price;
}

let laptop1 = new Laptop ("Aspire 3","Dell", 25000);
console.log(laptop1);
let laptop2 = new Laptop ("XPS 13","Dell", 45000);
console.log(laptop2);


let person = {
 name: "Slim Shady",
 talk: function(){
 	console.log("My name is what? My name is who? " +this.name);

 }

}

let person2 = {

	name: "Dr. Dre",
	greet: function(friend){

		// console.log(friend);
		console.log("Good day, " +friend.name);
	}

}

// person.talk();
person2.greet(person);

// Constructor with a built in method

function Dog(name,breed){

	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
			console.log("Bark! Bark!, " +friend.name);
	}

}

let dog1 = new Dog("Rocky", "Bulldog");
console.log(dog1);
dog1.greet(person);
dog1.greet(person2);


let dog2 = new Dog("Blackie", "Rottweiler");
console.log(dog2);
dog2.greet(dog1);
dog2.greet(dog1);
